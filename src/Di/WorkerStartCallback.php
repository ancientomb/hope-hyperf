<?php

declare(strict_types=1);

namespace NanQi\Hope\Di;

use Hyperf\Framework\Bootstrap\WorkerStartCallback as BaseWorkerStartCallback;
use Swoole\Server;

/**
 * 解决swoole随机数问题
 * Class WorkerStartCallback
 * @package NanQi\Hope\Di
 */
class WorkerStartCallback extends BaseWorkerStartCallback
{
    /**
     * Handle Swoole onWorkerStart event.
     */
    public function onWorkerStart(Server $server, int $workerId)
    {
        parent::onWorkerStart($server, $workerId);

        mt_srand();
        srand();
    }
}
