<?php

declare(strict_types=1);

namespace NanQi\Hope\Di;

use Hyperf\Contract\StdoutLoggerInterface;
use Hyperf\Utils\Filesystem\Filesystem;
use Hyperf\Utils\Str;
use Hyperf\Watcher\Option;
use Swoole\Coroutine\Channel;
use Swoole\Timer;
use Symfony\Component\Finder\SplFileInfo;

class ScanFileDriver extends \Hyperf\Watcher\Driver\ScanFileDriver
{
    protected function getWatchMD5(&$files): array
    {
        $filesMD5 = [];
        $filesObj = [];
        $dir = $this->option->getWatchDir();
        $ext = $this->option->getExt();
        // Scan all watch dirs.
        foreach ($dir as $d) {
            $path = BASE_PATH . '/' . $d;
            if (is_dir($path)) {
                $filesObj = array_merge($filesObj, $this->filesystem->allFiles($path));
            }
        }
        /** @var SplFileInfo $obj */
        foreach ($filesObj as $obj) {
            $pathName = $obj->getPathName();
            if (Str::endsWith($pathName, $ext)) {
                $files[] = $pathName;
                $contents = file_get_contents($pathName);
                $filesMD5[$pathName] = md5($contents);
            }
        }
        // Scan all watch files.
        $file = $this->option->getWatchFile();
        $filesObj = $this->filesystem->files(BASE_PATH, true);
        /** @var SplFileInfo $obj */
        foreach ($filesObj as $obj) {
            $pathName = $obj->getPathName();
            if (Str::endsWith($pathName, $file)) {
                $files[] = $pathName;
                $contents = file_get_contents($pathName);
                $filesMD5[$pathName] = md5($contents);
            }
        }
        return $filesMD5;
    }
}
