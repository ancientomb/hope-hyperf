<?php

declare(strict_types=1);

namespace NanQi\Hope\Di;

use Hyperf\Crontab\Crontab;

class CrontabManager extends \Hyperf\Crontab\CrontabManager
{
    public function register(Crontab $crontab): bool
    {
        //重复名字的cron不允许
        if (isset($this->crontabs[$crontab->getName()])) {
            return false;
        }

        return parent::register($crontab);
    }
}
