<?php

declare(strict_types=1);

namespace NanQi\Hope\Constants;

use NanQi\Hope\Base\BaseConstants;
use Hyperf\Constants\Annotation\Constants;

/**
 * @Constants
 */
class CacheSyncConstants extends BaseConstants
{
    const SYNC_CACHE_KEY = "sync:%s:%s";
}
