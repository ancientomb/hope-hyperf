<?php

declare(strict_types=1);

namespace NanQi\Hope\Constants;

use NanQi\Hope\Base\BaseConstants;
use Hyperf\Constants\Annotation\Constants;

/**
 * @Constants
 */
class ConfigConstants extends BaseConstants
{
    /**
     * Merge the config using array_merge_recursive.
     */
    public const CONFIG_MERGE = 1;

    /**
     * Replace the config with array_merge.
     */
    public const CONFIG_REPLACE = 1 << 1;
}
