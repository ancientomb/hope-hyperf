<?php


namespace NanQi\Hope\Db;


use Elasticsearch\Client;
use Hyperf\Elasticsearch\ClientBuilderFactory;
use NanQi\Hope\Helper;

class Es
{
    use Helper;

    public static function getClient() : Client
    {
        /** @var ClientBuilderFactory $clientBuilderFactory */
        $clientBuilderFactory = di(ClientBuilderFactory::class);
        $builder = $clientBuilderFactory->create();

        $host = config('hope.es-host', '192.168.10.1');
        $port = config('hope.es-port', 9200);
        $query = $builder
            ->setHosts([
                $host . ':' . $port
            ]);
        $esName = config('hope.es-name', '');
        $esPwd = config('hope.es-pwd', '');
        if ($esName && $esPwd) {
            $query = $query->setBasicAuthentication($esName, $esPwd);
        }
        $client = $query->build();
        return $client;
    }

    public static function query($index = null)
    {
        return new EsQuery($index);
    }

    /**
     * __callStatic
     * @param $func
     * @param $params
     * @return EsQuery
     */
    public static function __callStatic($func, $params)
    {
        $query = static::query();
        return $query->$func(...$params);
    }
}