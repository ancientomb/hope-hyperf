<?php

declare(strict_types=1);

namespace NanQi\Hope\Db;

use NanQi\Hope\Base\BaseModel;
use NanQi\Hope\Traits\OptimisticLockingTrait;

abstract class SecretModel extends BaseModel
{
    use OptimisticLockingTrait;

    /**
     * @var string 标识字段名
     */
    public $idFieldName = 'user_id';
    /**
     * @var string 签名字段名
     */
    public $signFieldName = 'sign';

    protected $hidden = ['lock_version', 'sign'];

    public function getSignData() : array
    {
        return [
            'token' => $this->token
        ];
    }
}
