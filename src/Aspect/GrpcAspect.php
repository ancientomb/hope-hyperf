<?php
declare(strict_types = 1);

namespace NanQi\Hope\Aspect;

use Hyperf\Di\Annotation\AnnotationCollector;
use Hyperf\Di\Aop\ProceedingJoinPoint;
use NanQi\Hope\Annotation\Grpc;
use NanQi\Hope\Base\BaseAspect;
use NanQi\Hope\Ext\GrpcHandler;

class GrpcAspect extends BaseAspect
{
    public $annotations = [
        Grpc::class,
    ];

    /**
     * @param ProceedingJoinPoint $proceedingJoinPoint
     * @return mixed
     * @throws \Throwable
     */
    public function process(ProceedingJoinPoint $proceedingJoinPoint)
    {
        /** @var Grpc $annotation */
        $className = $proceedingJoinPoint->className;

        $annotation = AnnotationCollector::getClassAnnotation($className, Grpc::class);
        if (! $annotation) {
            throw new \RuntimeException("grpc error");
        }

        /** @var GrpcHandler $handler */
        $handler = di($annotation->handler);

        return $handler->handle($proceedingJoinPoint, $annotation);
    }
}