<?php
declare(strict_types = 1);

namespace NanQi\Hope\Aspect;

use Hyperf\Di\Aop\ProceedingJoinPoint;
use Hyperf\Di\Exception\Exception;
use Hyperf\HttpMessage\Stream\SwooleStream;
use Hyperf\HttpServer\Response;
use Hyperf\Utils\Codec\Json;
use NanQi\Hope\Base\BaseAspect;
use NanQi\Hope\Traits\ResponseFormatTrait;

/**
 * Class ResponseAspect
 * @deprecated 不再使用切面完成此功能
 * @package NanQi\Hope\Aspect
 */
class ResponseAspect extends BaseAspect
{
    use ResponseFormatTrait;

	public $classes = [
	    Response::class . '::json'
	];

    /**
	 * @param ProceedingJoinPoint $proceedingJoinPoint
	 *
	 * @return mixed
	 * @throws Exception
	 */
	public function process(ProceedingJoinPoint $proceedingJoinPoint)
	{
        $response = $proceedingJoinPoint->process();

        $oldData = Json::decode($response->getBody()->getContents(), true);
        $newBody = $this->successFormat($oldData);

		return $response->withBody(new SwooleStream($newBody));
	}
}