<?php

declare(strict_types=1);

namespace NanQi\Hope\Factory;

use NanQi\Hope\Base\BaseJob;

class MqFactory
{
    public function create(string $name, \Closure $closure): BaseJob
    {
        return new class($name, $closure) extends BaseJob {
            private $closure;

            public function __construct(string $name, \Closure $closure)
            {
                parent::__construct($name);
                $this->closure = $closure;
            }

            public function handle()
            {
                call($this->closure);
            }
        };
    }
}
