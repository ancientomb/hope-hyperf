<?php

namespace NanQi\Hope;

use Hyperf\Contract\ConfigInterface;
use Hyperf\Contract\StdoutLoggerInterface;
use Hyperf\Crontab\CrontabManager;
use Hyperf\ExceptionHandler\Handler\WhoopsExceptionHandler;
use Hyperf\ExceptionHandler\Listener\ErrorExceptionHandler;
use Hyperf\HttpServer\Exception\Handler\HttpExceptionHandler;
use Hyperf\Validation\Middleware\ValidationMiddleware;
use NanQi\Hope\Exception\Handler\AppExceptionHandler;
use NanQi\Hope\Exception\Handler\BusinessExceptionHandler;
use NanQi\Hope\Exception\Handler\EsNotFoundExceptionHandler;
use NanQi\Hope\Exception\Handler\HttpNotFoundExceptionHandler;
use NanQi\Hope\Exception\Handler\NotFoundExceptionHandler;
use NanQi\Hope\Exception\Handler\StaleModelExceptionHandler;
use NanQi\Hope\Exception\Handler\ValidationExceptionHandler;

class ConfigProvider
{
    public function __invoke(): array
    {
        return [
            'aspects'=>[
            ],
            'dependencies' => [
                StdoutLoggerInterface::class => Di\StdoutLogger::class,
                CrontabManager::class => Di\CrontabManager::class,

//                ConfigInterface::class => Di\ConfigFactory::class,
            ],
            'annotations' => [
                'scan' => [
                    'paths' => [
                        __DIR__,
                    ],
                ],
            ],
            'commands' => [
            ],
            'listeners' => [
                ErrorExceptionHandler::class
            ],
            'middlewares' => [
                'http' => [
                    ValidationMiddleware::class
                ],
            ],
            'exceptions' => [
                'handler' => [
                    'http' => [
                        BusinessExceptionHandler::class,
                        NotFoundExceptionHandler::class,
                        EsNotFoundExceptionHandler::class,
                        HttpNotFoundExceptionHandler::class,
                        HttpExceptionHandler::class,
                        ValidationExceptionHandler::class,
                        WhoopsExceptionHandler::class,
                        AppExceptionHandler::class,
                        StaleModelExceptionHandler::class,
                    ],
                ],
            ],
            'publish' => [
                [
                    'id' => 'config',
                    'description' => 'Hope config', // 描述
                    // 建议默认配置放在 publish 文件夹中，文件命名和组件名称相同
                    'source' => __DIR__ . '/../publish/hope.php',  // 对应的配置文件路径
                    'destination' => BASE_PATH . '/config/autoload/hope.php', // 复制为这个路径下的该文件
                ],
            ],
        ];
    }
}
