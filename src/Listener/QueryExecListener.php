<?php

declare(strict_types = 1);

namespace NanQi\Hope\Listener;

use Hyperf\Database\Events\QueryExecuted;
use Hyperf\Event\Annotation\Listener;
use Hyperf\Utils\Arr;
use Hyperf\Utils\Context;
use Hyperf\Utils\Str;
use NanQi\Hope\Base\BaseListener;
use NanQi\Hope\Helper;

class QueryExecListener extends BaseListener
{
    use Helper;

	public function listen() : array
	{
		return [
			QueryExecuted::class,
		];
	}

	/**
	 * @param object $event
	 */
	public function process(object $event) : void
	{
	    $isProd = $this->isProduct();
		if ($event instanceof QueryExecuted && !$isProd) {
            $sql = $event->sql;
            if (! Arr::isAssoc($event->bindings)) {
                foreach ($event->bindings as $key => $value) {
                    $sql = Str::replaceFirst('?', "'{$value}'", $sql);
                }
            }
			$eventSqlList   = (array)Context::get(class_basename(__CLASS__));
			$eventSqlList[] = $sql;
			Context::set(class_basename(__CLASS__), $eventSqlList);
		}
	}
}
