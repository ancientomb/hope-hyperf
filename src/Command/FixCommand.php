<?php


namespace NanQi\Hope\Command;

use Hyperf\Command\Annotation\Command;
use InvalidArgumentException;
use NanQi\Hope\Base\BaseCommand;
use NanQi\Hope\Ext\FixInterface;
use Psr\Container\ContainerInterface;
use Symfony\Component\Console\Input\InputArgument;

/**
 * Class FixCommand
 * @Command()
 * @package NanQi\Hope\Command
 */
class FixCommand extends BaseCommand
{
    protected $name = 'fix';

    protected function getArguments()
    {
        return [
            ['method', InputArgument::REQUIRED, '执行方法'],
            ['args', InputArgument::IS_ARRAY, '参数'],
        ];
    }

    public function configure()
    {
        parent::configure();
        $this->setDescription('修复代码');
    }

    public function handle()
    {
        /** @var ContainerInterface $container */
        $container = di(ContainerInterface::class);

        if (!$container->has(FixInterface::class)) {
            throw new InvalidArgumentException();
        }

        /** @var FixInterface $fix */
        $fix = di(FixInterface::class);

        $method = $this->input->getArgument('method');
        if (!method_exists($fix, $method)) {
            throw new InvalidArgumentException();
        }

        $args = $this->input->getArgument('args');
        $fix->$method(...$args);
    }
}