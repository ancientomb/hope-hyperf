<?php
/**
 * author: NanQi
 * datetime: 2020/8/23 13:56
 */
namespace NanQi\Hope\Crontab;

interface CronInterface
{
    /**
     * cron执行入口
     * @return mixed
     */
    public function execute();
}