<?php
/**
 * author: NanQi
 * datetime: 2020/8/23 13:59
 */

namespace NanQi\Hope\Crontab\Batch;

use NanQi\Hope\Helper;
use NanQi\Hope\Hope;

abstract class BaseBatch implements BatchInterface, BatchPipeInterface
{
    use Helper;

    public function getCronClass() : string
    {
        $class = get_called_class();

        $cronClass = $class . 'Cron';
        if (class_exists($cronClass)) {
            return $cronClass;
        }
        $cronClass = str_replace('Batch', 'BatchCron', $class);
        if (class_exists($cronClass)) {
            return $cronClass;
        }

        throw new \RuntimeException('没有找到Cron类');
    }

    public function pipe($data, $fromWorkerId)
    {
        $flg = $this->execute($data);
        if ($flg) {
            $this->getServer()->sendMessage(new BatchPipeMessage(
                $this->getCronClass(), $this->getServer()->getWorkerId()
            ), $fromWorkerId);
        } else {
            //重试机制
        }
    }
}