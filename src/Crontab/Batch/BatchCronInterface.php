<?php
/**
 * author: NanQi
 * datetime: 2020/8/23 13:56
 */
namespace NanQi\Hope\Crontab\Batch;

use Iterator;

interface BatchCronInterface
{
    /**
     * 生成器
     * @return Iterator
     */
    public function dispatch() : Iterator;

    /**
     * 获取Batch的Class
     * @return string
     */
    public function getBatchClass() : string;
}