<?php

declare(strict_types=1);

namespace NanQi\Hope\Crontab\Batch;

class BatchPipeMessage
{
    /**
     * @var string
     */
    public $callable;

    public $data;

    public function __construct($callable, $data)
    {
        $this->callable = $callable;
        $this->data = $data;
    }
}
