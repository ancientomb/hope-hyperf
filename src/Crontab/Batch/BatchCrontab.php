<?php
/**
 * author: NanQi
 * datetime: 2020/8/23 17:09
 */

namespace NanQi\Hope\Crontab\Batch;


use Hyperf\Crontab\Crontab;
use NanQi\Hope\Helper;

/**
 * Class BatchCrontab
 * @deprecated 不再使用
 * @package NanQi\Hope\Crontab\Batch
 */
class BatchCrontab extends \NanQi\Hope\Crontab\Crontab
{
    use Helper;

    /**
     * 设置BatchCron
     * @param string $batchCronClass
     * @return Crontab
     */
    public function setCron(string $batchCronClass)
    {
        $instance = di($batchCronClass);
        if (!$instance instanceof BaseBatchCron) {
            throw new \InvalidArgumentException('$batchCronClass 必须继承 BaseBatchCron');
        }

        return $this->setCallback([$batchCronClass, 'execute']);
    }
}