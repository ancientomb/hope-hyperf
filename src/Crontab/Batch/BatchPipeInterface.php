<?php
/**
 * author: NanQi
 * datetime: 2020/8/23 13:56
 */
namespace NanQi\Hope\Crontab\Batch;

interface BatchPipeInterface
{
    /**
     * 消息处理
     * @param $data
     * @param $fromWorkerId
     */
    public function pipe($data, $fromWorkerId);
}