<?php
/**
 * author: NanQi
 * datetime: 2020/8/23 13:56
 */
namespace NanQi\Hope\Crontab\Batch;

interface BatchInterface
{
    /**
     * 具体执行方法
     * @param $data
     * @return bool
     */
    public function execute($data) : bool;

    /**
     * 获取Cron的Class
     * @return string
     */
    public function getCronClass() : string;
}