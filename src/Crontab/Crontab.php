<?php

declare(strict_types=1);

namespace NanQi\Hope\Crontab;

use Hyperf\Crontab\Crontab as BaseCrontab;
use NanQi\Hope\Helper;

class Crontab extends BaseCrontab
{
    use Helper;

    /**
     * 设置Cron
     * @param string $cronClass
     * @return Crontab
     */
    public function setCron(string $cronClass)
    {
        /** @var Crontab $instance */
        $instance = di($cronClass);
        if (!$instance instanceof CronInterface) {
            throw new \InvalidArgumentException('$cronClass 必须继承 CronInterface');
        }

        return $this->setCallback([$cronClass, 'execute']);
    }

    /**
     * @var array
     */
    protected $nextCrontabList = [];

    public function getNextCrontabList()
    {
        return $this->nextCrontabList;
    }

    /**
     * @var bool
     */
    protected $isShowLog = true;

    /**
     * @return bool
     */
    public function getIsShowLog(): bool
    {
        return $this->isShowLog;
    }

    /**
     * @param bool $isShowLog
     */
    public function setIsShowLog(bool $isShowLog): void
    {
        $this->isShowLog = $isShowLog;
    }

    /**
     * 设置完成后下一个执行的任务
     * @param string $crontabClass
     * @return Crontab
     */
    public function setNextCrontab(string $crontabClass) : Crontab
    {
        $this->nextCrontabList[] = $crontabClass;
        return $this;
    }
}
