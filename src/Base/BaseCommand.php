<?php

declare(strict_types=1);

namespace NanQi\Hope\Base;

use Hyperf\Command\Command;
use NanQi\Hope\Helper;

abstract class BaseCommand extends Command
{
    use Helper;
}
