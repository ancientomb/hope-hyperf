<?php

declare(strict_types=1);

namespace NanQi\Hope\Base;

use Hyperf\Di\Annotation\Inject;
use NanQi\Hope\Helper;
use Psr\EventDispatcher\EventDispatcherInterface;

/**
 * Class BaseEsDao
 * @deprecated 不强制使用Dao代码分层
 * @package NanQi\Hope\Base
 */
abstract class BaseEsDao extends BaseDao
{
    use Helper;

}
