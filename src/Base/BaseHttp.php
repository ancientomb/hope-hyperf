<?php


namespace NanQi\Hope\Base;

use Hyperf\Guzzle\ClientFactory;
use NanQi\Hope\Helper;

abstract class BaseHttp extends BaseRpc
{
    protected $client;

    public function __construct(ClientFactory $clientFactory)
    {
        $this->client = $clientFactory->create([]);
    }
}