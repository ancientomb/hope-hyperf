<?php

declare(strict_types=1);

namespace NanQi\Hope\Base;

use Hyperf\Di\Annotation\Inject;
use NanQi\Hope\Crontab\CronInterface;
use NanQi\Hope\Helper;
use Psr\EventDispatcher\EventDispatcherInterface;

abstract class BaseCron implements CronInterface
{
    use Helper;

    public function getCrontabName()
    {
        $class = get_called_class();
        return str_replace('Cron', '', class_basename($class));
    }
}
