<?php

declare(strict_types=1);

namespace NanQi\Hope\Base;

use Hyperf\Di\Annotation\Inject;
use Hyperf\Redis\Redis;
use NanQi\Hope\Helper;

/**
 * Class BaseRedisDao
 * @deprecated 不强制使用Dao代码分层
 * @package NanQi\Hope\Base
 */
abstract class BaseRedisDao extends BaseDao
{
    use Helper;
}
