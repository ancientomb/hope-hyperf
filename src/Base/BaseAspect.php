<?php

declare(strict_types=1);

namespace NanQi\Hope\Base;

use Hyperf\Di\Annotation\AbstractAnnotation;
use Hyperf\Di\Annotation\AnnotationCollector;
use Hyperf\Di\Aop\AbstractAspect;
use Hyperf\Di\Aop\ProceedingJoinPoint;
use NanQi\Hope\Helper;

abstract class BaseAspect extends AbstractAspect
{
    use Helper;

    /**
     * 获取 Annotation 对象
     * @deprecated 此方法局限于获取MethodAnnotation，不再推荐使用
     * @param ProceedingJoinPoint $proceedingJoinPoint
     * @param string $annotationClass
     * @return AbstractAnnotation
     */
    public function getAnnotation(ProceedingJoinPoint $proceedingJoinPoint, string $annotationClass) : AbstractAnnotation
    {
        $className = $proceedingJoinPoint->className;
        $method = $proceedingJoinPoint->methodName;

        $collector = AnnotationCollector::get($className);
        $annotation = $collector['_m'][$method][$annotationClass] ?? null;

        return $annotation;
    }
}
