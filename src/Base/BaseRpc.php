<?php


namespace NanQi\Hope\Base;

use Hyperf\CircuitBreaker\FallbackInterface;
use Hyperf\Di\Aop\ProceedingJoinPoint;
use NanQi\Hope\Helper;

abstract class BaseRpc implements FallbackInterface
{
    use Helper;

    public function fallback(ProceedingJoinPoint $proceedingJoinPoint)
    {
        $this->errorBusy();
    }
}