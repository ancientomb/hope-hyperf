<?php

declare(strict_types=1);

namespace NanQi\Hope\Base;

use NanQi\Hope\Ext\FixInterface;
use NanQi\Hope\Helper;

abstract class BaseFix implements FixInterface
{
    use Helper;

}
