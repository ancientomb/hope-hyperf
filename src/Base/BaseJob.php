<?php

declare(strict_types=1);

namespace NanQi\Hope\Base;

use Hyperf\AsyncQueue\Job;
use NanQi\Hope\Helper;

abstract class BaseJob extends Job
{
    use Helper;

}
