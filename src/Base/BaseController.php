<?php

declare(strict_types=1);

namespace NanQi\Hope\Base;

use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Contract\RequestInterface;
use Hyperf\HttpServer\Contract\ResponseInterface;
use NanQi\Hope\Helper;
use Psr\Container\ContainerInterface;

abstract class BaseController
{
    use Helper;

    /**
     * @Inject
     * @var RequestInterface
     */
    protected $request;

    /**
     * @Inject
     * @var ResponseInterface
     */
    protected $response;

    /**
     * 返回成功
     * @deprecated 可直接返回，不需要调用
     * @param $data
     * @param $transformer
     * @return object
     */
    public function retSuccess($data = null, $transformer = null)
    {
        return $this->response->json($data);
    }

    /**
     * 获取登录的用户ID
     */
    public function getLoginId()
    {
        return $this->getAuthId();
    }
}
