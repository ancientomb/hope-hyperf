<?php

declare(strict_types=1);

namespace NanQi\Hope\Base;

use Hyperf\AsyncQueue\Driver\DriverFactory;
use Hyperf\AsyncQueue\JobInterface;
use NanQi\Hope\Helper;

/**
 * Class BaseMqDao
 * @deprecated 不强制使用Dao代码分层
 * @package NanQi\Hope\Base
 */
abstract class BaseMqDao extends BaseDao
{
    use Helper;

    /**
     * 投递消息
     * @deprecated 直接使用 $this->mq
     * @param JobInterface $job
     * @param int $delay
     * @param string $channel
     * @return bool
     */
    public function produce(JobInterface $job, int $delay = 0, string $channel = 'default') : bool
    {
        /** @var DriverFactory $driverFactory */
        $driverFactory = di(DriverFactory::class);
        return $driverFactory->get($channel)->push($job, $delay);
    }
}
