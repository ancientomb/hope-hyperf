<?php

declare(strict_types=1);

namespace NanQi\Hope\Base;

use Hyperf\Event\Contract\ListenerInterface;
use NanQi\Hope\Helper;

abstract class BaseListener implements ListenerInterface
{
    use Helper;

}
