<?php

namespace NanQi\Hope\Exception;

use NanQi\Hope\Base\BaseException;

class StaleModelLockingException extends BaseException {
    protected $message = "数据库乐观锁出错";

    /**
     * @var string
     */
    protected $table;

    public function getTable()
    {
        return $this->table;
    }

    public function __construct(string $table)
    {
        parent::__construct($this->message);

        $this->table= $table;
    }
}
