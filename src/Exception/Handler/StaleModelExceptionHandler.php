<?php

declare(strict_types=1);

namespace NanQi\Hope\Exception\Handler;

use Hyperf\Database\Model\ModelNotFoundException;
use Hyperf\Validation\ValidationException;
use NanQi\Hope\Constants\StatusCodeConstants;
use NanQi\Hope\Exception\StaleModelLockingException;
use Psr\Http\Message\ResponseInterface;
use Throwable;

class StaleModelExceptionHandler extends BaseExceptionHandler
{
    public function handle(Throwable $throwable, ResponseInterface $response)
    {
        $this->stopPropagation();

        /** @var StaleModelLockingException $throwable */
        return $this->errorResponse($response,
            StatusCodeConstants::S_407_BUSY,
            $throwable->getMessage(),
            [
                'table' => $throwable->getTable(),
                'trace' => $throwable->getTrace()
            ]);
    }

    public function isValid(Throwable $throwable): bool
    {
        return $throwable instanceof StaleModelLockingException;
    }
}
