<?php

declare(strict_types=1);

namespace NanQi\Hope\Exception\Handler;

use Hyperf\HttpMessage\Exception\NotFoundHttpException;
use NanQi\Hope\Constants\StatusCodeConstants;
use Psr\Http\Message\ResponseInterface;
use Throwable;

class HttpNotFoundExceptionHandler extends BaseExceptionHandler
{
    public function handle(Throwable $throwable, ResponseInterface $response)
    {
        $this->stopPropagation();

        /** @var NotFoundHttpException $throwable */
        return $this->errorResponse($response,
            StatusCodeConstants::S_404_NOT_FOUND,
            "没有找到对应的路由",
            [
                'uri' => $this->getReq()->getUri(),
                'full' => $this->getReq()->fullUrl(),
                'trace' => $throwable->getTrace()
            ]);
    }

    public function isValid(Throwable $throwable): bool
    {
        return $throwable instanceof NotFoundHttpException;
    }
}
