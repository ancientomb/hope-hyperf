<?php

declare(strict_types=1);

namespace NanQi\Hope\Annotation;

use NanQi\Hope\Ext\GrpcHandler;

/**
 * @Annotation
 * @Target({"CLASS"})
 */
class Grpc extends Hystrix
{
    /**
     * 服务名
     * @var string
     */
    public $svc;

    public $handler = GrpcHandler::class;

    public function __construct($value = null)
    {
        parent::__construct($value);
        if (is_string($value)) {
            $this->svc = $value;
        } else if (is_array($value)) {
            $this->value = $value ?? [];
        }
    }
}
