<?php

declare(strict_types=1);

namespace NanQi\Hope\Annotation;

use Hyperf\Di\Annotation\AbstractAnnotation;

/**
 * @Annotation
 * @Target({"METHOD"})
 */
class MultiTransactional extends AbstractAnnotation
{

    /**
     * 连接池
     * @var string
     */
    public $pool;

    public function __construct($value = null)
    {
        parent::__construct($value);
        if (is_string($value)) {
            $this->pool = $value;
        }
    }
}
