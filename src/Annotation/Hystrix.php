<?php

declare(strict_types=1);

namespace NanQi\Hope\Annotation;

use Hyperf\CircuitBreaker\Annotation\CircuitBreaker;
use Hyperf\CircuitBreaker\FallbackInterface;
use Hyperf\Di\Annotation\AnnotationCollector;
use Hyperf\Di\Aop\ProceedingJoinPoint;
use NanQi\Hope\Helper;

/**
 * @Annotation
 * @Target({"METHOD"})
 */
class Hystrix extends CircuitBreaker implements FallbackInterface
{
    use Helper;

    /**
     * @var float
     */
    public $timeout = 3;

    /**
     * @var string
     */
    public $fallback = Hystrix::class.'::fallback';

    /**
     * The counter required to reset to a close state.
     * @var int
     */
    public $successCounter = 5;

    /**
     * The counter required to reset to a open state.
     * @var int
     */
    public $failCounter = 5;

    public function collectMethod(string $className, ?string $target): void
    {
        AnnotationCollector::collectMethod($className, $target, CircuitBreaker::class, $this);
    }

    public function fallback(ProceedingJoinPoint $proceedingJoinPoint)
    {
        $this->errorBusy();
    }
}