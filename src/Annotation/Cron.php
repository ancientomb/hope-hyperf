<?php

declare(strict_types=1);

namespace NanQi\Hope\Annotation;

use Hyperf\Di\Annotation\AbstractAnnotation;

/**
 * @Annotation
 * @Target({"CLASS"})
 */
class Cron extends AbstractAnnotation
{
    /**
     * @var string
     */
    public $rule;

    /**
     * @var array
     */
    public $nextList = [];
}
