<?php

declare(strict_types=1);

namespace NanQi\Hope\Annotation;

use Hyperf\Di\Annotation\AbstractAnnotation;

/**
 * @Annotation
 * @Target({"METHOD"})
 */
class Lockable extends AbstractAnnotation
{

    /**
     * @var string
     */
    public $prefix;

    /**
     *
     * @var string
     */
    public $value;

    /**
     * 过期时间
     * @var int
     */
    public $ttl = 5;

    /**
     * 是否等待
     * @var bool
     */
    public $isWait = false;

    /**
     * 等待时间
     * @var int
     */
    public $uSleep = 200;

    public function __construct($value = null)
    {
        parent::__construct($value);
        $this->ttl = (int) $this->ttl;
        $this->isWait = (bool) $this->isWait;
        $this->uSleep = (int) $this->uSleep;
    }
}
