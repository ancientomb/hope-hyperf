<?php
/**
 * author: NanQi
 * datetime: 2020/10/18 12:27
 */

return [
    'es-host' => env('ES_HOST', '192.168.10.1'),
    'es-port' => env('ES_HOST', 9200),
    'es-name' => env('ES_NAME', ''),
    'es-pwd' => env('ES_PWD', ''),
    'api-sign' => env('API_SIGN', 'nanqi'),
    'jwt-secret' => env('JWT_SECRET', 'NanQi'),
    'jwt-ttl' => env('JWT_TTL', 86400),
    'cron-sync-buffer' => env('CRON_SYNC_BUFFER', '*/10 * * * *'),
    'db-aes-key' => env('DB_AES_KEY', 'nanqi'),
];